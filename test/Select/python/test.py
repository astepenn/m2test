import FWCore.ParameterSet.Config as cms

process = cms.Process("Demo")
process.load("FWCore.MessageService.MessageLogger_cfi")

process.load("Configuration.StandardSequences.SimulationRandomNumberGeneratorSeeds_cff")
process.load('Configuration.StandardSequences.Services_cff')
process.load('Configuration/StandardSequences/FrontierConditions_GlobalTag_cff')
from Configuration.AlCa.GlobalTag import GlobalTag


#process.load("Configuration.StandardSequences.Simulation_cff")
process.load("Configuration.StandardSequences.Reconstruction_cff")

#process.load("Configuration.StandardSequences.FrontierConditions_GlobalTag_cff")
from Configuration.AlCa.GlobalTag import GlobalTag

process.GlobalTag = GlobalTag(process.GlobalTag, '106X_dataRun2_v32', '')

process.maxEvents = cms.untracked.PSet( input = cms.untracked.int32(-1) )

process.source = cms.Source("PoolSource",
    fileNames = cms.untracked.vstring(
#'file:/eos/cms/store/group/dpg_hcal/comm_hcal/USC/run346428/USC_346428.root'
#'/store/data/Commissioning2022/MinimumBias/RAW/v1/000/347/687/00000/83e14e9e-1ce8-4cd7-858b-d3402963fcf3.root'
#'file:/afs/cern.ch/user/a/astepenn/timing/CMSSW_12_2_1/src/test/Select/reco_RAW2DIGI_RECO_M2.root'
'file:/afs/cern.ch/work/a/astepenn/HCAL/CMSSW_10_6_20/src/my_config_RAW2DIGI_RECO_largeChi2.root'
#'/store/mc/Run3Winter22DR/QCD_Pt-15to7000_TuneCP5_Flat_13p6TeV-pythia8/GEN-SIM-DIGI-RAW/PUForTRK_DIGI_122X_mcRun3_2021_realistic_v9-v2/2820000/01044c71-ca95-4cb4-9515-55b911241ef2.root'
  )
#eventsToProcess = cms.untracked.VEventRange('275886:47707831')
)

process.dump=cms.EDAnalyzer('EventContentAnalyzer')
#process.hbhereco = process.hbheprereco.clone()

process.demo = cms.EDAnalyzer('Select',
                                        isMC = cms.bool(False)
                      #                  updatedPatJetsUpdatedJEC = cms.InputTag("updatedPatJetsTransientCorrectedUpdatedJEC")
)


process.p = cms.Path( 
#process.dump *
 process.demo)

